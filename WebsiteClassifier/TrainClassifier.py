"""
Script Description:
Trains a website classifier that uses one of three models.
Can train:
    1. Naive Bayes Classifier
    2. Four three layer feed forward neural networks
    3. Support Vector Machine (Linear Kernel)
"""
import sys
import h5py
import pickle
import sklearn
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.multioutput import MultiOutputClassifier
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping
from sklearn.naive_bayes import GaussianNB
import numpy as np

EPOCHS = 15

INPUT_DIR = "./convDump"

# Load the data

# File containing the website data
h5f = h5py.File('./convDump/features/dat_new_negativetfidf.h5', 'r')

X = h5f["X"][:]
Y = h5f["Y"][:]

h5f.close()

# Partition test and training set.

X_svm, X_test_svm, y_svm, y_test_svm = train_test_split(
    X, Y[:,1],
    test_size=0.3333,
    random_state=42
)

X, X_test, y, y_test = train_test_split(
    X, Y,
    test_size=0.3333,
    random_state=42
)

print(len(Y[:,1]))


if sys.argv[1] == "bayes":
    print("Gaussian Naive Bayes:")
    print("#####################")
    clf = GaussianNB()
    clf = MultiOutputClassifier(clf)
    clf.fit(X, y)
    score = sklearn.metrics.precision_recall_fscore_support(
        y_test,
        clf.predict(X_test),
        average='macro')
    print("Test Score: ",score)
    score = sklearn.metrics.precision_recall_fscore_support(
        y,
        clf.predict(X),
        average='macro')

    print("Train Score: ", score)
    print(",".join([str(i) for i in score[:3]]))
    with open('./convDump/models/bayes.pkl', 'wb+') as f:
        pickle.dump(clf, f)

    print("")

elif sys.argv[1] == "neural":

    print("3 Layer NN:")
    print("#####################")

    early_stop = EarlyStopping(monitor='loss', min_delta=0, patience=3, verbose=1, mode='auto')
    for i in range(0,69):
        
        print ("************ Training Classifier "+ str(i)+" ************")
        y_second = y[:, i]

        model = Sequential([
            Dense(min(512, X.shape[1]), input_dim=X.shape[1]),
            Activation('relu'),
            Dense(min(512, X.shape[1])),
            Activation('relu'),
            Dropout(0.2),
            Dense(1),
            Activation('sigmoid')
        ])

        model.compile(optimizer='adam',
                      loss='binary_crossentropy',
                      metrics=['binary_accuracy'])

        print("Fitting Model")
        model.fit(X, y_second, epochs=EPOCHS, batch_size=32, verbose=1, callbacks=[early_stop])
        predTemp = model.predict_classes(X_test, batch_size=32, verbose=0)
        pred = predTemp
        y_test_second = y_test[:, i]
        score = sklearn.metrics.precision_recall_fscore_support(
            y_test_second,
            pred,
            average='macro')

        print(",".join([str(i) for i in score[:3]]))

        model.save("./convDump/models/neuralnet_newNegative_topic%s.h5" % i)

    print("")

elif sys.argv[1] == "svm":
    print("SVM (Linear Kernel):")
    clf = svm.SVC(kernel='linear')
    clf = MultiOutputClassifier(clf)

    #labels = np.unique(y)
    #print (labels)

    y_svm = y_svm.reshape((-1,1))

    print (X_svm.shape)
    print (y_svm.shape)

    clf.fit(X_svm, y_svm)
    predictions = clf.predict(X_test_svm)
    score = sklearn.metrics.precision_recall_fscore_support(
            y_test_svm,
            predictions,
            average='macro')
    print("Test Scores", score)
    score = sklearn.metrics.precision_recall_fscore_support(
            y_svm,
            clf.predict(X_svm),
            average='macro')
    print("Train Scores: ", score)
    print(",".join([str(i) for i in score[:3]]))
    with open('./convDump/models/svcl_less.pkl', 'wb+') as f:
        pickle.dump(clf, f)

    print("")
