"""
Script Description:
Creates feature data for a given pair of query and predictions created by LabelQueries.py
"""
import json
import pickle
import numpy as np
import gensim
import h5py
import re


# Create a list of strs corresponding to each label dump we will be working with

dir_str = "./QueryLabels/"


#strs = [dir_str + "0-99.json", dir_str + "100-199.json", dir_str + "200-299.json"]
strs = [dir_str + "labeldump900-999.json"]

dat = {
    "X": None,
    "Y": None,
}

words = []

print("Loading word2vec model!")
model = gensim.models.KeyedVectors.load_word2vec_format(
    '../../GoogleNews-vectors-negative300.bin.gz', binary=True)


pattern = re.compile(r"(?is)<script[^>]*>(.*?)</script>")

# Loop through all data entries in the json files


for string in strs:
    with open(string) as f:
        d = json.load(f)

    topic_dat = np.zeros((len(d["labels"]), 300))
    i = 0

    #ind[100] = 0

    for doc in d["labels"]:
        predictions = doc["add"]["predictions"]

        # For now, we are going to take the % of positive predictions for each IV
        inds = np.zeros(70)
        for entry in range(len(predictions)):
            for index in range(0,69):
                if predictions[entry][index] >= 0.5:
                    inds[index] += 1

        for index in range(0,69):
            inds[index] = inds[index]/len(predictions)


        """
        ind0 = 0
        ind1 = 0
        ind2 = 0
        ind3 = 0

        for entry in range(len(predictions)):

            if predictions[entry][0] >= 0.5:
                ind0 += 1
            if predictions[entry][1] >= 0.5:
                ind1 += 1
            if predictions[entry][2] >= 0.5:
                ind2 += 1
            if predictions[entry][3] >= 0.5:
                ind3 += 1

        ind0 = ind0 / len(predictions)
        ind1 = ind1 / len(predictions)
        ind2 = ind2 / len(predictions)
        ind3 = ind3 / len(predictions)
        t = np.array([[ind0, ind1, ind2, ind3]])
        """
        t = np.copy(inds)
        print(t)
        
        if dat["Y"] is None:
            dat["Y"] = t
        else:
            dat["Y"] = np.concatenate((dat["Y"], t), axis=0)

        # Then we will populate dat["X"] with a mean of word embeddings.

        query = doc["add"]["query"]

        c = 0
        for tok in query.split():
            if tok in model:
                c += 1
                topic_dat[i] += model[tok]
        if c > 0:
            topic_dat[i] /= float(c)
        else:
            print("None of these words are in the model!")
            print(doc["add"]["query"])

        i += 1

        words.append(doc["add"]["query"])

    if dat["X"] is None:
        dat["X"] = topic_dat
    else:
        dat["X"] = np.concatenate((dat["X"], topic_dat), axis=0)


print(dat["Y"].shape)
print(dat["X"].shape)
print(len(words))

# Save the data
h5f = h5py.File('./QueryLabels/LabelData.h5', 'w')
for k, v in dat.items():
    h5f.create_dataset(k, data=v)
h5f.close()

with open('./QueryLabels/InitialQueriesLabelData.pkl', 'wb') as f:
    pickle.dump(words, f, -1)
