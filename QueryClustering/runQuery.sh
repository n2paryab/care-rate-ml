#!/bin/bash

echo "LabelQueries ..."
python3 LabelQueries.py  
echo "Done."

echo "LabelProcDump ..."
python3 LabelProcDump.py
echo "Done."

echo "QueryClusterer"
python3 QueryClusterer.py
echo "Done."
