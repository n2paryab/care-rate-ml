"""
Uses four keras neural networks to make predictions on the same set of data.
"""
import numpy as np
import keras


class GroupNeuralModel(object):
	"""
	Uses four keras neural networks to make predictions on the same set of data.
	"""

	"""
	def __init__(self, symptoms, riskfactors, daycare, treatment):
		self.symptoms = keras.models.load_model(symptoms)
		self.riskfactors = keras.models.load_model(riskfactors)
		self.daycare = keras.models.load_model(daycare)
		self.treatment = keras.models.load_model(treatment)
	"""
	# # we 69 labels
	def __init__(self, models=None):
		self.models = models
		for index in range(0,len(models)):
			self.models[index] = keras.models.load_model(models[index])

	def predict_classes(self, info_vecs):
		
		"""
		:param info_vecs: Data to predict on, representing web pages.
		:type info_vecs: numpy array
		:return: numpy array of predictions for each web page, divided by category, rounded to either 0 or 1.
		:rtype: numpy array
		"""
		
		"""
		pred1 = self.symptoms.predict_classes(info_vecs)
		pred2 = self.treatment.predict_classes(info_vecs)
		pred3 = self.daycare.predict_classes(info_vecs)
		pred4 = self.riskfactors.predict_classes(info_vecs)

		predictions = np.zeros((info_vecs.shape[0], 4))

		for entry in range(pred1.shape[0]):
			predictions[entry] = np.concatenate((pred1[entry], pred4[entry], pred3[entry], pred2[entry]), axis=0)
		"""

		self.preds_classes = np.zeros((info_vecs.shape[0], 69))
		for index in range(0,69):
		   self.preds_classes[:,index] = self.models[index].predict_classes(info_vecs).ravel()

		predictions = np.zeros((info_vecs.shape[0], 69))
		"""
		all_arr1 = np.zeros((1, 70))
		for entry in range(self.preds[:,0].shape[0]):
			all_arr1 = self.preds[:,1][entry]
			for index in range(2,70):
				all_arr1 = np.concatenate((all_arr1 , self.preds[index,entry]), axis=0)
			predictions[entry] = all_arr1
		"""
		predictions = self.preds_classes 
		return predictions

	def predict(self, info_vecs):
		"""
		:param info_vecs: Data to predict on, representing web pages.
		:type info_vecs: numpy array
		:return: numpy array of predictions for each web page, divided by category, with no rounding.
		:rtype: numpy array
		"""

		"""
		pred1 = self.symptoms.predict(info_vecs)
		pred2 = self.treatment.predict(info_vecs)
		pred3 = self.daycare.predict(info_vecs)
		pred4 = self.riskfactors.predict(info_vecs)

		predictions = np.zeros((info_vecs.shape[0], 4))

		for entry in range(pred1.shape[0]):
			predictions[entry] = np.concatenate((pred1[entry], pred4[entry], pred3[entry], pred2[entry]), axis=0)
		"""
		self.preds = np.zeros((info_vecs.shape[0], 69))
		for index in range(0,69):
		   self.preds[:,index] = self.models[index].predict(info_vecs).ravel()

		predictions = np.zeros((info_vecs.shape[0], 69))
		"""
		all_arr1 = np.zeros((1, 70))
		for entry in range(self.preds[:,0].shape[0]):
			all_arr1 = self.preds[:,1][entry]
			for index in range(2,70):
				all_arr1 = np.concatenate((all_arr1 , self.preds[index,entry]), axis=0)
			predictions[entry] = all_arr1
		"""
		predictions = self.preds 
		return predictions

