#!/bin/bash

# Given two files containing lists of URLS and corresponding labels, create a json file with the text from each webpage, as well as the label.
echo "pyconv is running ..."
python3 pyconv.py 

# Takes a json file produced with pyconv.py and pickles a file that has partitioned the data into two arrays.
# One array holds the body of the webpage.
# The other holds the labels for that webpage.
echo "proc_dump is running ..."
python3 proc_dump.py > proc_dump.txt
echo "Done."

#Takes data from a .pkl file from proc_dump.py and encodes it into one of three feature types: tfidf, count, word2vec  
echo "prep_dat.py tfidf ..."
python3 prep_dat.py tfidf > tfidf.txt
echo "Done."

echo "prep_dat.py count ..."
python3 prep_dat.py count > count.txt
echo "Done."

echo "prep_dat.py word2vec ..."
python3 prep_dat.py word2vec > word2vec.txt
echo "Done."

# Train three models with TFID features
echo "TrainClassifier.py bayes ..."
python3 TrainClassifier.py bayes > bayes.txt
echo "Done."

echo "TrainClassifier.py neural ..."
python3 TrainClassifier.py neural > neural.txt
echo "Done."

# this fails
echo "TrainClassifier.py svm ..."
python3 TrainClassifier.py svm > svm.txt
echo "Done."
