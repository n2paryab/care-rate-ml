pyconv.py
Script Description:
Given two files containing lists of URLS and corresponding labels, create a json file
with the text from each webpage, as well as the label.
To be used with proc_dump.py .
"""

proc_dump.py
Script Description:
Takes a json file produced with pyconv.py and pickles a file that has partitioned the data into two arrays.
One array holds the body of the webpage.
The other holds the labels for that webpage.
"""

prep_dat.py (tfidf, tfidf, count)
Script Description:
Takes data from a .pkl file from proc_dump.py and encodes it into one of three feature types.
"""

TrainClassifier.py (bayes, neural, svm)
Script Description:
Trains a website classifier that uses one of three models.
Can train:
    1. Naive Bayes Classifier
    2. Four three layer feed forward neural networks
    3. Support Vector Machine (Linear Kernel)
"""
